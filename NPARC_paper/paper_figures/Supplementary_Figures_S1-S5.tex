\documentclass[oneside]{amsart}
\usepackage[margin=3cm]{geometry}
\usepackage{subcaption}
\usepackage{graphicx}

\newcommand{\methodname}{NPARC}  % Nonparametric analysis of response curves
\newcommand{\tm}{$T_{\mathrm{m}}$}
\newcommand{\oldmethodname}{the \tm-based approach}  
\renewcommand{\thesubfigure}{\Alph{subfigure}}
\newcommand{\subfigref}[1]{\subref{#1}}
\newcommand{\longeqref}[1]{(Eq. \eqref{#1})}


\newcommand{\beginsupplement}{%
        \setcounter{table}{0}
        \renewcommand{\thetable}{S\arabic{table}}%
        \setcounter{figure}{0}
        \renewcommand{\thefigure}{S\arabic{figure}}%
        \setcounter{section}{1}
        \renewcommand{\thesection}{S\arabic{section}}%
     }

\begin{document}
\beginsupplement

% S1:
\begin{figure}[b!]
\centering 
    \includegraphics[width = 15cm]{tpp_tr_schematic_revised}
\caption{ Experimental workflow of TPP experiments.
  After ligand (orange) or vehicle (grey) treatment, the cells or cell extracts are split up into ten aliquots and heated to different temperatures.
  This is followed by protein extraction, trypsin digestion and labeling with TMT10 isobaric tags specific to each temperature.
  Protein abundances are quantified by the reporter ion intensities in the LC-MS/MS.
  Finally, fold changes are computed that reflect the protein concentration relative to the lowest temperature. 
  \label{FigS1:TPP-workflow}
  }
\end{figure}

\clearpage

% S2:
\begin{figure}[b!]
\centering 
    \includegraphics[width = 15cm]{Fig_S2}
\caption{Effects of QC-filters (Table 3) applied to restrict the analysis to `well-behaved' curves for hypothesis testing in the \tm-based approach of [6]. The numbers and colors indicate the fraction of proteins in each dataset that is removed from the analysis by each filter.
  \label{FigS2:QC-filters}
  }
\end{figure}

% S3:
\begin{figure}[b!]
\raggedright % fill subfigures from the left
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Panobinostat_Frankenetal_2015_ACTR10.pdf}
\caption{\label{FigS3:ACTR10}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Panobinostat_Frankenetal_2015_C5orf51.pdf}
\caption{\label{FigS3:C5orf51}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Panobinostat_Frankenetal_2015_GNB1L.pdf}
\caption{\label{FigS3:GNB1L}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Panobinostat_Frankenetal_2015_GTF2B.pdf}
\caption{\label{FigS3:GTF2B}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Panobinostat_Frankenetal_2015_NUP93.pdf}
\caption{\label{FigS3:NUP93}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Panobinostat_Frankenetal_2015_RNASEH2C.pdf}
\caption{\label{FigS3:RNASEH2C}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Panobinostat_Frankenetal_2015_SMTN.pdf}
\caption{\label{FigS3:SMTN}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Panobinostat_Frankenetal_2015_WDR26.pdf}
\caption{\label{FigS3:WDR26}}
\end{subfigure}
\caption{Melting curves of the eight panobinostat hits detected with Benjamini-Hochberg adjusted p-values $\leq 0.01$ in addition to the expected targets in Figure 1.
  \label{FigS3:additional-panob-hits}}
\end{figure}

% S4:
\begin{figure}[b!]
\raggedright % fill subfigures from the left
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Dasatinib_Savitskietal_2014_CRKL.pdf}
\caption{\label{FigS4:CRKL}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Dasatinib_Savitskietal_2014_YES1.pdf}
\caption{\label{FigS4:YES1}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Dasatinib_Savitskietal_2014_AKAP9.pdf}
\caption{\label{FigS4:AKAP9}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Dasatinib_Savitskietal_2014_GAB2.pdf}
\caption{\label{FigS4:GAB2}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Dasatinib_Savitskietal_2014_MAPK14.pdf}
\caption{\label{FigS4:MAPK14}}
\end{subfigure}
\caption{
The \methodname\ approach provides the flexibility to simultaneously compare multiple experimental conditions as illustrated by the treatment with dasatinib at two different concentrations. 
\textbf{(\subfigref{FigS4:CRKL})} - \textbf{(\subfigref{FigS4:YES1})}: Crk-like protein (CRKL), and Src family tyrosine kinase (YES1) are interaction partners of the fusion protein BCR-ABL and are destabilized already at small concentrations of dasatinib. \textbf{(\subfigref{FigS4:AKAP9})} - \textbf{(\subfigref{FigS4:MAPK14})}: AKAP9, GAB2 and MAPK14 show subtle, dose-dependent but highly reproducible effects on the thermostability after  dasatinib treatment. % \citep{Rix2007chemical} 
All proteins were detected with Benjamini-Hochberg adjusted F-test p-values $\leq 0.01$ (Supplementary Table S1). 
}
\label{FigS:examples-dasatinib}
\end{figure}

% S5:
\begin{figure}[!b]
\raggedright % fill subfigures from the left
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Staurosporine_Savitskietal_2014_PRKCA.pdf}
\caption{\label{FigS5:PRKCA}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Staurosporine_Savitskietal_2014_PRKCD.pdf}
\caption{\label{FigS5:PRKCD}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Staurosporine_Savitskietal_2014_PRKCE.pdf}
\caption{\label{FigS5:PRKCE}}
\end{subfigure}
%
\begin{subfigure}{5cm}
    \includegraphics[width = 5cm]{Fig_protein_Staurosporine_Savitskietal_2014_PRKCQ.pdf}
\caption{\label{FigS5:PRKCQ}}
\end{subfigure}
\caption{
\label{FigS5:examples-stauro-PRKC-proteins}
Further examples of the protein kinase C family for which treatment effects are poorly reflected by shifts in \tm.
Similar to PRKCB (Fig. 1C), the strongest destabilizing effect appears to take place at cooler temperatures than the actual \tm\ value for PRKCD, PRKCE and PRKCQ. All proteins except PRKCA were detected with Benjamini-Hochberg adjusted F-test p-values $\leq 0.01$ (Supplementary Table S1).}
\end{figure} 

\end{document}