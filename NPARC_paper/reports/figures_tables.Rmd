---
title: "Non-parametric analysis of thermal proteome profiles: Figures and Tables"
author: "Dorothee Childs, Nils Kurzawa"
date: "`r format(Sys.time(), '%d %B %Y,   %X')`"
bibliography: bibliography.bib
output: 
  BiocStyle::pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, cache = FALSE)
```

# Introduction
This document shows how to reproduce the Figures and Tables shown in [Childs, Bach, Franken et al. (2018): Non-parametric analysis of thermal proteome profiles reveals novel drug-binding proteins.](https://www.biorxiv.org/content/early/2018/07/22/373845).


# Preparation

Load necessary packages:
```{r dependencies, message=FALSE}
library(tidyverse)
library(knitr)
library(grid)
library(ggthemes)
library(cowplot)
library(ROCR)
library(openxlsx)
```

# Data

First we load the relevant data:

- TPP experiment data [@Franken2015, @Reinhard2015, @Savitski2014], preprocessed as shown in script `NPARC_workflow.Rmd`, 
- NPARC results, computed in script `NPARC_workflow.Rmd`,
- Results of the Tm-based approach, computed by the Bioconductor package `TPP`.

```{r load_data}
tppData <- readRDS("../data/tppData_preprocessed.Rds")
nparcResults <- readRDS("../data/resultsNPARC.Rds")
tmResults <- readRDS("../data/resultsTm.Rds")
```


# Functions
```{r}
theme_Paper <- function(base_size=12, base_family="") {
  #Theme used for all plots
  (theme_foundation(base_size=base_size, base_family=base_family)
   + theme(plot.title = element_text(face = "bold", size = rel(1.2), hjust = 0.5),
           text = element_text(),
           panel.background = element_rect(colour = NA),
           plot.background = element_rect(colour = NA),
           axis.title = element_text(face = "bold",size = rel(0.8)),
           axis.title.y = element_text(angle=90,vjust =2, size=rel(1.2)),
           axis.title.x = element_text(vjust = -0.2, size=rel(1.2)),
           axis.text = element_text(size=rel(1.05)), 
           axis.line.x = element_line(colour="black"),
           axis.line.y = element_line(colour="black"),
           axis.ticks = element_line(),
           panel.grid.major = element_blank(),
           panel.grid.minor = element_blank(),
           legend.key = element_rect(colour = NA),
           legend.direction = "vertical",
           legend.justification=c(1,1),
           legend.position = "right",
           legend.key.size= unit(0.8, "cm"),
           legend.title = element_blank(),
           plot.margin=unit(c(10,5,5,5),"mm"),
           strip.background = element_rect(fill = "white", color = "white")
           
   ))
}

reverselog_trans <- function(base = exp(1)) {
  trans <- function(x) -log(x, base)
  inv <- function(x) base^(-x)
  scales::trans_new(paste0("reverselog-", format(base)), trans, inv, 
                    scales::log_breaks(base = base), 
                    domain = c(-Inf, Inf))
}

fitSingleSigmoid <- function(x, y, start=c(Pl = 0, a = 550, b = 10)){
  try(nls(formula= y ~ (1 - Pl)  / (1+exp((b - a/x))) + Pl, 
          start=start, 
          data=list(x=x, y=y),
          na.action = na.exclude, 
          algorithm = "port",
          lower = c(0.0, 1e-5, 1e-5), 
          upper = c(1.5, 15000, 250),
          control = nls.control(maxiter=50)), 
      silent = TRUE)
}

repeatFits <- function(x, y, start = c(Pl = 0, a = 550, b = 10), 
                       seed = NULL, alwaysPermute = FALSE, maxAttempts = 100){
  
  i <- 0
  doFit <- TRUE
  doVaryPars <- alwaysPermute
  
  if (!is.null(seed)){
    set.seed(seed)
  }
  
  while (doFit){
    startTmp <- start * (1 + doVaryPars*runif(1, -0.5, 0.5))
    m <- fitSingleSigmoid(x = x, y = y, start = startTmp)
    i <- i + 1
    doFit <- inherits(m, "try-error") & i < maxAttempts
    doVaryPars <- TRUE
  }
  
  return(m)
}

plotMeltCurves <- function(data, proteinID, datasetID, lineWidth = 1, pointSize = 1){
  
  subs <- data %>%
    mutate(geneName = gsub("_IPI.*", "", uniqueID)) %>%
    filter(dataset == datasetID, geneName == proteinID)
  
  xNew <- seq(min(subs$temperature), max(subs$temperature), length.out = 100)
  
  alternativePredictions <- subs %>%
    group_by(compoundConcentration) %>%
    do({
      fit = repeatFits(x = .$temperature, 
                       y = .$relAbundance)
      tibble(relAbundance = predict(fit, newdata = list(x = xNew)), x = xNew)
    }) %>% ungroup 

  
  plt <- ggplot(subs, aes(x = temperature, y = relAbundance, 
                          color = factor(compoundConcentration))) +
    theme_bw() +
    theme_Paper() +
    theme(text = element_text(size=12), 
          plot.margin = margin(0.5, 0.5, 0.5, 0.5, unit = "cm")) + 
    facet_wrap(~ geneName) +
    # ggtitle(proteinID) +
    xlab("Temperature [\U00B0 C]") +
    ylab("Fraction\nnon-denatured") + 
    scale_color_manual("Condition") +
    scale_shape_manual("Replicate", values = c(16, 16)) +
    scale_y_continuous(breaks = seq(0, 1, by = 0.5)) +
    geom_line(data = alternativePredictions, aes(x = x), size = lineWidth) +
    geom_point(aes(shape = factor(replicate)), size = pointSize)
  
  return(plt)
}

computeROC <- function(scores, labels){
  # score can be fdr estimates, p-values, or any other score for which 
  # small values should be assigned to class 1, and larger values to class 0.
  scores[is.na(scores)] <- Inf
  
  if (length(unique(labels)) == 2){
    pred <- ROCR::prediction(-scores, labels)
    
    rocCurve <- data.frame(threshold = -pred@cutoffs[[1]], fp = pred@fp[[1]], tp = pred@tp[[1]]) %>%
      mutate(fpr = fp / max(fp), tpr = tp / max(tp)) %>%
      arrange(threshold)
    
  } else {
    rocCurve <- data.frame(threshold = scores, fp = NA, tp = NA, fpr = NA, tpr = NA)
  }
  
  return(rocCurve)
  
}
```

# Fig. 1E
```{r fig1E}
# Annotate labels for fill aesthetics:
pltData <- tmResults %>%
  mutate(label = ifelse(!is.na(tm_V_repl1) & !is.na(tm_V_repl2) & !is.na(tm_T_repl1) & !is.na(tm_T_repl2), 
                        "Tm available\n", "Tm outside\nmeasured\nrange\n")) %>%
  mutate(label = ifelse(label == "Tm available\n" & !passes_all_req, 
                        "Criteria on melting\ncurve not fulfilled\n",
                        label)) %>%
  mutate(label = factor(label, levels = unique(label)[c(2,1,3)]))

# Annotate x-labels by sampl size
pltData <- pltData %>%
  group_by(dataset) %>%
  mutate(N = n()) %>%
  ungroup %>%
  mutate(dataset = paste0(dataset, "\nN = ", N))

bar_plt <- ggplot(pltData) + 
  theme_bw() + xlab("") + ggtitle(" ") +
  theme(legend.position="right", legend.direction = "vertical", panel.grid.major.x = element_blank()) + 
  theme(plot.margin = margin(0.1, 0.1, 0.1, 0.1, unit = "cm")) +
  theme(text = element_text(size=10)) +
  scale_y_continuous(labels = paste(seq(0,100, by = 20)), breaks = seq(0,100, by = 20)/100, name = "%") +
  geom_bar(aes(x = dataset, fill = label), stat = "count", position="fill", width = 0.6) +
  coord_cartesian(ylim = c(-0.05, 1.05)) +
  guides(fill = guide_legend(title = NULL)) +
  scale_fill_manual(values = c(RColorBrewer::brewer.pal(n = 12, name = "Set3")[c(4, 2)],
                               RColorBrewer::brewer.pal(9, name = "Set1")[9]))

cowplot::save_plot(filename = "../paper_figures/Fig_1E.pdf", 
                   plot = bar_plt, 
                   base_height = 3, 
                   base_width = 7) # 5.5 Inches = 14 cm

pltData %>% group_by(dataset, label, N) %>% summarize(n = n()) %>% spread(label, n)
```

![Fig. 1(E)](../paper_figures/Fig_1E.pdf)


# Fig. 2C
```{r fig2C}
# Data to plot:
pltData <-  nparcResults %>%
  filter(dataset == "Staurosporine") %>%
  mutate(rssDiff = rssDiff * s0_sq)

stauroHits <- filter(pltData, pAdj <= 0.01)
stk4Results <- filter(pltData, uniqueID == "STK4_IPI00011488")

# Volcano plot (p-values vs. RSS-differences):
outerPlt <- ggplot(pltData, aes(x = rssDiff, y = pVal)) +
  theme_Paper() +
  scale_x_log10(name = expression(paste(RSS[0] - RSS[1]))) +
  scale_y_continuous(trans = reverselog_trans(10)) +
  ylab("p-value") + 
  annotate("label", x = 5, y = 1e-15, label = "p[adj] <= 0.01", color = "red", 
           parse = TRUE) +
  geom_point(data = filter(pltData, pAdj > 0.01), alpha = 0.5) +
  geom_point(data = stauroHits, alpha = 1, color = "red", size = 1.5) +
  geom_point(data = stk4Results, color = "steelblue") +
  geom_segment(data = stk4Results, color = "steelblue",
               aes(x = rssDiff - 1, xend = rssDiff - 0.1, yend = pVal),
               arrow = arrow(length = unit(3,"mm"))) +
  geom_label(data = stk4Results, aes(x = rssDiff - 1.05), 
             label = "STK4", color = "steelblue")

# Histogram plot (F-statistics):
innerPlt <- ggplot(pltData, aes(x = fStat)) +
  theme_Paper() +
  theme(panel.background = element_rect(fill = "gray96"), 
        plot.background = element_rect(color = "black")) +
  xlab("F-Statistic") + 
  ylab("Density") +
  annotate("text", x = 300, y = 0.4, parse = TRUE, 
           label="F==frac(d[2]*(RSS[0]-RSS[1]),d[1]*RSS[1])") +
  geom_histogram(aes(y = ..density..), binwidth = 0.5, color = "black") +
  geom_point(alpha=0.3, aes(y = -0.05), pch = "|", size=2.5) +
  geom_segment(data = stk4Results, aes(xend = fStat), color = "steelblue", 
               y = 0.2, yend = 0.01, arrow = arrow(length = unit(3,"mm"))) +
  geom_label(data = stk4Results, y = 0.25, label = "STK4", color = "steelblue") 
```

```{r save_fig2C, results="hide"}
# Overlay Volcano and histogram plots
pdf("../paper_figures/Fig_2C.pdf", width = 10, height = 5)
print(outerPlt); 
print(innerPlt, vp = grid::viewport(0.125, 0.9, 0.45, 0.45, just = c("left", "top")))
dev.off()
```

![Fig. 2 (C)](../paper_figures/Fig_2C.pdf)

```{r eval = FALSE, echo = FALSE}
```


# Fig. 4
```{r fig4}
# Select staurosporime data
pltData <-  nparcResults %>%
  filter(dataset == "Staurosporine") %>%
  mutate(rssDiff = rssDiff * s0_sq) %>%
  mutate(deltaTm = tm_20 - tm_0) # compute Tm-shift

# Annotate test results by Tm-based approach
pltData <- tmResults %>% 
  dplyr::select(dataset, uniqueID, knownTarget, fulfills_all_4_requirements) %>%
  right_join(pltData, by = c("dataset", "uniqueID")) 

# Add label for coloring/shape aesthetics
pltData <- pltData %>%
  mutate(nparcHit = pAdj <= 0.01) %>%
  mutate(shape = ifelse(knownTarget, "Annotated protein kinase", 
                        "Not an annotated protein\nkinase"),
         color = ifelse(nparcHit & fulfills_all_4_requirements, 
                        "Detected by both methods", NA),
         color = ifelse(nparcHit & !fulfills_all_4_requirements, 
                        "Only detected by NPARC", color),
         color = ifelse(!nparcHit & fulfills_all_4_requirements, 
                        "Only detected by Tm-based\napproach", color),
         color = ifelse(!nparcHit & !fulfills_all_4_requirements, 
                        "Not detected by either method", color))

# Only consider Tm estimates inside measured range
pltData <- filter(pltData, tm_0 >= 40, tm_20 <= 67)

# Define label coordinates
labelCoords <- pltData %>%
  mutate(geneID = gsub("_IPI.*", "", uniqueID)) %>%
  filter(geneID %in% c("STK4", "PRKCB", "FECH", "XPOT", "MAPK14", "MAPKAPK2")) %>%
  mutate(x = deltaTm + 6 * sign(deltaTm), y = rssDiff * 2,
         hjust = ifelse(deltaTm < 0, "right", "left"))

# Define shape and color scales
brewerCol <- RColorBrewer::brewer.pal(9, "Set1")
colors <- c("Detected by both methods" = brewerCol[3],
            "Only detected by NPARC" = brewerCol[1],
            "Only detected by Tm-based\napproach" = brewerCol[2],
            "Not detected by either method" = brewerCol[9])
shapes <- c("Annotated protein kinase" = 19, 
            "Not an annotated protein\nkinase" = 3)
  
# Create scatter plot
scatterPlt <- ggplot(pltData, aes(x = deltaTm, y = rssDiff, color = color, shape = shape)) + 
  theme_bw() +
  scale_x_continuous(limits = c(-15, 20), name = expression(paste(italic("T")[m](Staurosporine) - italic("T")[m](Vehicle)))) +
  scale_y_continuous(limits = c(0, 2.5), name = expression(paste(RSS[0] - RSS[1])),  minor_breaks = NULL) +
  coord_fixed(ratio = 15) +
  scale_color_manual(name = "", values = colors) +
  scale_shape_manual(name = "", values = shapes) +
  geom_point(alpha = 0.25, size = 1) +
  geom_point(data = filter(pltData, color != "Not detected by either method"), alpha = 0.75, size = 1) +
  geom_label(data = labelCoords, aes(label = geneID, x = x, y = y, hjust = hjust), 
             show.legend = FALSE, alpha = 1, size = 2, label.padding = unit(0.1, "lines")) +
  geom_segment(data = labelCoords, aes(x = x, y = y, xend = deltaTm, yend = rssDiff), 
               show.legend = FALSE, size = 0.5, alpha = 0.5) +
  theme(legend.text = element_text(size = 10), 
        panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank())

# Create density plots
xDens <- cowplot::axis_canvas(scatterPlt, axis = 'x') + 
  geom_density(data = filter(pltData, knownTarget, 
                             color != "Not detected by either method",
                             color != "Only detected by Tm-based\napproach"),
               aes(x = deltaTm, y = ..count.., fill = color), alpha = 0.5) +
  scale_fill_manual(name = "", values = colors)

yDens <- cowplot::axis_canvas(scatterPlt, axis = 'y', coord_flip = TRUE) + 
  geom_density(data = filter(pltData, knownTarget,
                             color != "Not detected by either method",
                             color != "Only detected by Tm-based\napproach"),
               aes(x = rssDiff, y = ..count.., fill = color), alpha = 0.5) +
  scale_fill_manual(name = "", values = colors) +
  coord_flip()

# Add density plots to axes
scatterDens <- scatterPlt %>% 
  cowplot::insert_xaxis_grob(xDens, grid::unit(0.1, "null"), position = "top") %>%
  cowplot::insert_yaxis_grob(yDens, grid::unit(0.1, "null"), position = "right")

# Create melting curves of example proteins
mapk14 <- plotMeltCurves(tppData, proteinID = "MAPK14", datasetID = "Staurosporine", 
                         lineWidth = 0.75, pointSize = 1.25) + 
  guides(color = "none", shape = "none") +
  theme(text = element_text(size = 8), 
        plot.margin = margin(0, 0, .1, 0, unit = "in"), 
        plot.title = element_text(size  = 16)) +
  scale_color_manual(values = c("#808080", "#da7f2d"))

mapkapk2 <- plotMeltCurves(tppData, proteinID = "MAPKAPK2", datasetID = "Staurosporine", 
                           lineWidth = 0.75, pointSize = 1.25) +
  guides(color = "none", shape = "none") +
  theme(text = element_text(size = 8), 
        plot.margin = margin(0, 0, .1, 0, unit = "in"),
        plot.title = element_text(size  = 16)) +
  scale_color_manual(values = c("#808080", "#da7f2d")) +
  ylab("")

xpot <- plotMeltCurves(tppData, proteinID = "XPOT", datasetID = "Staurosporine", 
                       lineWidth = 0.75, pointSize = 1.25) + 
  guides(color = "none", shape = "none") +
  theme(text = element_text(size = 8), 
        plot.margin = margin(0, 0, .1, 0, unit = "in"),
        plot.title = element_text(size  = 16)) +
  scale_color_manual(values = c("#808080", "#da7f2d"))


# Save plot
# pdf("../paper_figures/Fig_4.pdf", width = 10, height = 9)
pdf("../paper_figures/Fig4.pdf", width = 8, height = 7.5)
cowplot::ggdraw(scatterDens)
print(mapk14, vp = viewport(0.075, 0.8, 0.2, 0.2, just = c("left", "top")))
print(mapkapk2, vp = viewport(0.275, 0.8, 0.19, 0.2, just = c("left", "top")))
print(xpot, vp = viewport(0.075, 0.6, 0.2, 0.2, just = c("left", "top")))
dev.off()
```

![Fig. 4](../paper_figures/Fig4.pdf)

# Fig. 5A
```{r fig5A}
# Select staurosporime data
pltData <-  nparcResults %>% 
  filter(dataset == "Staurosporine") %>%
  dplyr::select(dataset, uniqueID, pAdj)

# Annotate test results by Tm-based approach
pltData <- tmResults %>% 
  dplyr::select(dataset, uniqueID, knownTarget, fulfills_all_4_requirements, p_adj_repl1, p_adj_repl2) %>%
  right_join(pltData, by = c("dataset", "uniqueID")) 

gatheredResults <- pltData %>% 
  gather(pValType, pAdj, pAdj, p_adj_repl1, p_adj_repl2) %>%
  mutate(pValType = plyr::mapvalues(x = pValType,
                                 from = c("pAdj",
                                          "p_adj_repl1", 
                                          "p_adj_repl2"), 
                                 to = c("NPARC", 
                                        "Tm-based test\n(replicate 1)\n", 
                                        "Tm-based test\n(replicate 2)\n"))) %>%
  filter(!is.na(pAdj))

roc <- gatheredResults %>% 
  group_by(dataset, pValType) %>%
  do(computeROC(scores = .$pAdj, labels = .$knownTarget)) %>%
  ungroup

starCoords <- pltData %>%
  distinct(uniqueID, dataset, fulfills_all_4_requirements, knownTarget) %>%
  group_by(dataset) %>%
  summarize(tp = sum( fulfills_all_4_requirements &  knownTarget, na.rm = TRUE),
            fp = sum( fulfills_all_4_requirements & !knownTarget, na.rm = TRUE),
            tn = sum(!fulfills_all_4_requirements & !knownTarget, na.rm = TRUE),
            fn = sum(!fulfills_all_4_requirements &  knownTarget, na.rm = TRUE)) %>%
  mutate(type = "Tm-based\nruleset")

pointCoords <- roc %>% 
  filter(threshold <= 0.01) %>%
  group_by(dataset, pValType) %>%
  mutate(maxThreshold = max(threshold)) %>%
  filter(threshold == maxThreshold) %>%
  mutate(type = "p[adj] <= 0.01") %>%
  ungroup

rocPlotStauro <- ggplot(data = roc, aes(x = fp, y = tp, color = pValType)) + 
  theme_Paper()  +
  geom_line(aes(x = fp, y = tp), na.rm = TRUE, size=1) +
  geom_point(data = starCoords, size = 3, stroke = 1, aes(x = fp, y = tp, shape = type), inherit.aes = FALSE) +
  geom_point(data = pointCoords, size = 3, stroke = 1, aes(shape = type)) +
  coord_equal(xlim = c(0, 100), ylim = c(0,100)) +
  scale_color_manual(values = c("darkred", "darkblue", "steelblue")) +
  scale_shape_manual(values = c(8, 16), 
                     labels=c("Tm-based\nruleset", parse(text="p[adj] <= 0.01"))) +
  ylab('Hits annotated as\nprotein kinases') +
  xlab('Hits not annotated\nas protein kinases') +
  ggtitle("Staurosporine") +
  theme(panel.grid.major = element_blank(),
        legend.text.align = 0,
        legend.key.size = unit(1.5, "cm"), 
        legend.key.width = unit(1, "cm"),
        plot.title = element_text(size = 14),
        text = element_text(size = 14),
        legend.text = element_text(size = 14))

cowplot::save_plot(filename = "../paper_figures/Fig_5A.pdf", 
                   plot = rocPlotStauro, 
                   base_height = 5, 
                   base_width = 6.2)
```

![Fig. 5A](../paper_figures/Fig_5A.pdf)


# Fig. 5B
```{r fig5B}
# Select staurosporime data
pltData <-  nparcResults %>% 
  filter(dataset == "ATP") %>%
  dplyr::select(dataset, uniqueID, pAdj)

# Annotate test results by Tm-based approach
pltData <- tmResults %>% 
  dplyr::select(dataset, uniqueID, knownTarget, fulfills_all_4_requirements, p_adj_repl1, p_adj_repl2) %>%
  right_join(pltData, by = c("dataset", "uniqueID")) 

gatheredResults <- pltData %>% 
  gather(pValType, pAdj, pAdj, p_adj_repl1, p_adj_repl2) %>%
  mutate(pValType = plyr::mapvalues(x = pValType,
                                 from = c("pAdj",
                                          "p_adj_repl1", 
                                          "p_adj_repl2"), 
                                 to = c("NPARC", 
                                        "Tm-based test\n(replicate 1)\n", 
                                        "Tm-based test\n(replicate 2)\n"))) %>%
  filter(!is.na(pAdj))

roc <- gatheredResults %>% 
  group_by(dataset, pValType) %>%
  do(computeROC(scores = .$pAdj, labels = .$knownTarget)) %>%
  ungroup

starCoords <- pltData %>%
  distinct(uniqueID, dataset, fulfills_all_4_requirements, knownTarget) %>%
  group_by(dataset) %>%
  summarize(tp = sum( fulfills_all_4_requirements &  knownTarget, na.rm = TRUE),
            fp = sum( fulfills_all_4_requirements & !knownTarget, na.rm = TRUE),
            tn = sum(!fulfills_all_4_requirements & !knownTarget, na.rm = TRUE),
            fn = sum(!fulfills_all_4_requirements &  knownTarget, na.rm = TRUE)) %>%
  mutate(type = "Tm-based\nruleset")

pointCoords <- roc %>% 
  filter(threshold <= 0.01) %>%
  group_by(dataset, pValType) %>%
  mutate(maxThreshold = max(threshold)) %>%
  filter(threshold == maxThreshold) %>%
  mutate(type = "p[adj] <= 0.01") %>%
  ungroup

rocPlotATP <- ggplot(data = roc, aes(x = fp, y = tp, color = pValType)) + 
  theme_Paper() +
  geom_line(aes(x = fp, y = tp), na.rm = TRUE, size=1) +
  geom_point(data = starCoords, size = 3, stroke = 1, aes(x = fp, y = tp, shape = type), inherit.aes = FALSE) +
  geom_point(data = pointCoords, size = 3, stroke = 1, aes(shape = type)) +
  coord_equal(xlim = c(0, 100), ylim = c(0,100)) +
  scale_color_manual(values = c("darkred", "darkblue", "steelblue")) +
  scale_shape_manual(values = c(8, 16), 
                     labels=c("Tm-based\nruleset", parse(text="p[adj] <= 0.01"))) +
  ylab('Hits annotated as\nATP binders') +
  xlab('Hits not annotated as\nATP binders') +
  ggtitle("ATP") +
  theme(panel.grid.major = element_blank(),
        legend.text.align = 0, 
        legend.key.size = unit(1.5, "cm"), 
        legend.key.width = unit(1, "cm"),
        plot.title = element_text(size = 14),
        text = element_text(size = 13),
        legend.text = element_text(size = 14))

cowplot::save_plot(filename = "../paper_figures/Fig_5B.pdf", 
                   plot = rocPlotATP, 
                   base_height = 5, 
                   base_width = 6.2)
```

![Fig. 5B](../paper_figures/Fig_5B.pdf)


# Fig. S2
```{r figS2}
proportionFiltered <- tmResults %>%
  group_by(dataset) %>%
  summarise(fails_r2_req = round(sum(!passes_req1_R2_greater_than_0.8_in_all_curves, na.rm = T)/n(), 2),
            fails_pl_req = round(sum(!passes_req2_Pl_less_than_0.3_in_vehicles, na.rm = T)/n(), 2),
            fails_slope_req = round(sum(!passes_req3_abs_slopes_steeper_than_0.06_in_all_curves, na.rm = T)/n(), 2),
            fails_at_least_1_req = round(sum(!passes_all_req, na.rm = T)/n(), 2))

proportionFilteredLong <- proportionFiltered %>%
  gather(filter_failed, percentage, fails_r2_req, fails_pl_req, fails_slope_req, fails_at_least_1_req) %>%
  mutate(label = plyr::mapvalues(filter_failed,
                                 from = c("fails_r2_req", "fails_pl_req", "fails_slope_req", "fails_at_least_1_req"),
                                 to = c("Filter 1\n (insufficient R^2)",
                                        "Filter 2\n (plateau of vehicle condition too high)",
                                        "Filter 3\n (shallow slope)",
                                        "At least one filter applies")))

plt <- ggplot(proportionFilteredLong) +
  theme(axis.text.x = element_text(angle = 45, hjust = 0.75, vjust = 0.8), 
        strip.background = element_blank(), strip.text = element_blank()) +
  ylab("Dataset") + xlab("Filter") +
  geom_tile(aes(x = label, y  = dataset, fill = percentage)) +
  scale_fill_gradientn(name = "Fraction of proteins\nin dataset", 
                       colors = c("white", rev(heat.colors(5))), 
                       limits = c(0, 0.35)) +
  geom_text(aes(x = label, dataset, label = percentage))

cowplot::save_plot(filename = "../paper_figures/Fig_S2.pdf",
                   plot = plt,
                   base_width = 8, 
                   base_height = 5)
```

![Fig. S2](../paper_figures/Fig_S2.pdf)


# Supplementary Table S1
```{r table_S1}
uniqueIDs <- tppData %>% 
  distinct(dataset, uniqueID) %>%
  separate(uniqueID, c("gene_symbol", "IPI"), "_", remove = FALSE)

combinedResults <- uniqueIDs %>%
  left_join(nparcResults, by = c("dataset", "uniqueID")) %>% 
  left_join(tmResults, by = c("dataset", "uniqueID")) %>%
  mutate(tm_vehicle = tm_0,
         tm_treatment = ifelse(dataset == "ATP", tm_2000, NA),
         tm_treatment = ifelse(dataset == "Panobinostat", tm_1, tm_treatment),
         tm_treatment = ifelse(dataset == "Staurosporine", tm_20, tm_treatment),
         `tm_treatment_0.5` = tm_0.5,
         `tm_treatment_5` = tm_5) %>%
  mutate(tm_diff = tm_treatment - tm_vehicle)

out <- combinedResults %>%
  arrange(dataset, uniqueID) %>%
  dplyr::select(gene_symbol, IPI, dataset, 
                is_a_known_target = knownTarget, 
                f_test_p_value = pVal, 
                f_test_p_adj = pAdj,
                f_statistic = fStat,
                obersavtions_used_for_model_fitting = n1, 
                tm_treatment, tm_treatment_0.5, tm_treatment_5,
                tm_vehicle, 
                tm_diff,
                RSS0 = rss0, 
                RSS1 = rss1, 
                fulfills_all_4_ad_hoc_filters = fulfills_all_4_requirements, 
                filter_1_Tm_diffs_same_sign = meltP_diffs_have_same_sign, 
                filter_2_Tm_diffs_T_vs_V_greater_V1_vs_V2 = meltP_diffs_T_vs_V_greater_V1_vs_V2, 
                filter_3_min_slopes = passes_req3_abs_slopes_steeper_than_0.06_in_all_curves, 
                filter_4_p_values = min_pVals_less_0.1_and_max_pVals_less_0.2, 
                z_test_p_adj_repl1 = p_adj_repl1, 
                z_test_p_adj_repl2 = p_adj_repl2, 
                fulfills_curve_fit_filters_repl1 = passed_filter_T1_vs_V1, 
                fulfills_curve_fit_filters_repl2 = passed_filter_T2_vs_V2, 
                tm_diff_repl1 = delta_tm_repl1, 
                tm_diff_repl2 = delta_tm_repl2,
                tm_vehicle_repl1 = tm_V_repl1, 
                tm_vehicle_repl2 = tm_V_repl2, 
                tm_treatment_repl1 = tm_T_repl1, 
                tm_treatment_repl2 = tm_T_repl2, 
                r2_vehicle_curve_repl1 = r2_V_repl1, 
                r2_vehicle_curve_repl2 = r2_V_repl2, 
                r2_treatment_curve_repl1 = r2_T_repl1, 
                r2_treatment_curve_repl2 = r2_T_repl2, 
                plateau_treatment_curve_repl1 = pl_T_repl1, 
                plateau_treatment_curve_repl2 = pl_T_repl2, 
                plateau_vehicle_curve_repl1 = pl_V_repl1, 
                plateau_vehicle_curve_repl2 = pl_V_repl2, 
                slope_vehicle_curve_repl1 = slope_Vehicle_1, 
                slope_vehicle_curve_repl2 = slope_Vehicle_2, 
                slope_treatment_curve_repl1 = slope_Treatment_1, 
                slope_treatment_curve_repl2 = slope_Treatment_2)

distr_pars <- combinedResults %>%
  distinct(dataset, s0_sq, d1, d2) %>%
  na.omit() %>%
  dplyr::rename(dataset = dataset,
                chi_sq_scaling_factor = s0_sq, 
                d1 = d1, 
                d2 = d2)

explanation_sheet <- data.frame(column_name = c("is_a_known_target",
                                                "f_test_p_value",
                                                "f_test_p_adj",
                                                "f_statistic",
                                                "obersavtions_used_for_model_fitting",
                                                "tm_[condition]",
                                                "tm_diff",
                                                "RSS1", 
                                                "RSS0",
                                                "chi_sq_scaling_factor",
                                                "d1",
                                                "d2",
                                                "fulfills_all_4_ad_hoc_filters",
                                                "filter_[number]",
                                                "z_test_p_adj_[replicate]",
                                                "fulfills_curve_fit_filters_[replicate]",
                                                "tm_diff_[replicate]",
                                                "tm_[condition]_[replicate]",
                                                "r2_[condition]_[replicate]",
                                                "plateau_[condition]_[replicate]",
                                                "slope_[condition]_[replicate]"),
                                description = c("Protein has been annotated as a known target based on GO terms or manual curation",
                                                "F-test p-value calculated from f_statistic_rescaled using the estimated degrees of freedom (d1, d2)",
                                                "Benjamini-Hochberg adjusted F-test p-value",
                                                "F-statistic used for p-value calculation. Calculated as ((RSS0-RSS1)/d1)/(RSS1/d2) ",
                                                "Number of observations per treatment condition to which curve could be fitted",
                                                "Melting point (vehicle or treatment condition), calculated from the alternative model fits",
                                                "Difference between melting points (Treatment-Vehicle), calculated from the alternative model fits",
                                                "Sum of squared residuals of the null model fit",
                                                "Sum of squared residuals of the alternative model fit",
                                                "Scaling parameter of the Chi2 distribution for the numerator and denominator of the F-statistic. Estimated from the mean and variance of  (RSS0-RSS1)",
                                                "DOF of the F-statistic numerator. Estimated by fitting a Chi-square distribution to (RSS0-RSS1)/Chi_sq_scaling_factor",
                                                "DOF of the F-statistic denominator. Estimated by fitting a Chi-square distribution to RSS1/Chi_sq_scaling_factor",
                                                "Protein fulfills the four ad-hoc filters on the test results of the Tm-based approach.",
                                                "Protein fulfills specific ad-hoc filter",
                                                "Benjamini-Hochberg adjusted z-test p-value for the given replicate",
                                                "Protein qualifies for p-value calculation in the given replicate",
                                                "Tm-shift (Treatment-Vehicle) calculated for the given replicate by the Tm-based approach",
                                                "Melting point (vehicle or treatment condition) calculated for the given replicate by the Tm-based approach",
                                                "R-squared (vehicle or treatment condition) calculated for the given replicate by the Tm-based approach",
                                                "Lower horizontal asymptote (vehicle or treatment condition) calculated for the given replicate by the Tm-based approach",
                                                "Slope at inflection point (vehicle or treatment condition) calculated for the given replicate by the Tm-based approach"),
                                method = c("", rep("NPARC", 11), rep("Tm-based approach", 9)))

out_list <- split(out, out$dataset)
out_list[["Estimated_distribution_parameters"]] <- distr_pars
out_list <- c(list(info = explanation_sheet), out_list)

write.xlsx(out_list, file = "../paper_supplements/Table_S1_draft.xlsx")
```


# Bibliography